import Vue from 'vue';
import App from './App.vue';
import store from './store.js';

require('./assets/scss/main.scss');

new Vue({
    el: '#app',
    render: h => h(App),
    store,
})