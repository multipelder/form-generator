export default {
    data() {},
    props: {
        input_id: String,
    },
    computed: {
        field() {
            return this.$store.getters.fields[this.input_id];
        },
    },
}