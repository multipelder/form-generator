import Vue from 'vue';
import Vuex from 'vuex';

import regModule from './modules/reg.js';

Vue.use(Vuex);

const store = new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: [
        regModule,
    ],
})

export default store;