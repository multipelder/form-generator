import formData from '../formData.js';

export const mutations = {
    SET_FIELDS: 'modules/reg/SET_FIELDS',
    SET_FIELDSGROUPS: 'modules/reg/SET_FIELDSGROUPS',
    UPDATE_FIELD: 'modules/reg/UPDATE_FIELD',
};

export const actions = {
    initFields: 'modules/reg/initFields',
};

export default {
    state: {
        fieldsGroups: [],
        fields: {},
    },

    mutations: {
        [mutations.SET_FIELDS]: (state, fields) => {
            fields.forEach((field) => {
                state.fields[field.id] = field;
            });
        },
        [mutations.SET_FIELDSGROUPS]: (state, fieldsGroups) => {
            state.fieldsGroups = fieldsGroups;
        },
        [mutations.UPDATE_FIELD]: (state, payload) => {
            state.fields[payload.id][payload.property] = payload.value;
        },
    },

    actions: {
        [actions.initFields]: ({ commit }, payload) => {
            commit(mutations.SET_FIELDS, formData.fields);
            commit(mutations.SET_FIELDSGROUPS, formData.fieldsGroups);
        },
    },

    getters: {
        fields(state) {
            console.log(state.fields);
            return state.fields;
        },
        fieldsGroups(state) {
            return state.fieldsGroups;
        }
    }
};
